#ifndef MYTREE_H
#define MYTREE_H
#include <iostream>
#include <vector>
#include <iterator>

using namespace std;

template <typename T>
class Bintree
{
private:
    struct Leaf
    {
        T data;
        int level = 0;
        Leaf* parent;
        Leaf* right;
        Leaf* left;
        Leaf();
        Leaf(const T& b,Leaf *a);
    };
    Leaf* root;
    void h_show_tree(Leaf *a, int level) const;
    void add(const T& j, Leaf *root);
    void add_leaf(const T& value);
    void delet_tree(Leaf* i);
    void remove_leaf_h(Leaf *node, const T&x);
    Leaf* min_search_h(Leaf* root)
    {
        Leaf* search = root;;
        if(root->left != nullptr)
        {
            min_search_h(root->left);
        }
        else
        {
            cout << search->data << endl;
            return search;
        }
        return nullptr;
    }
    Leaf* max_search_h(Leaf* root)
    {
        Leaf* search = root;;
        if (root->right != nullptr)
        {
            max_search_h(root->right);
        }
        else
        {
            cout << search->data << endl;
            return search;
        }
        return nullptr;
    }
    Leaf* search(const T& x, Leaf *a)
    {
        while (a != nullptr)
        {
            if (a->data == x)
            {
                return a;
            }
            else
            {
                if (a->data > x)
                    return search(x, a->left);
                else
                    return search(x, a->right);
            }
        }
        return nullptr;
    }
public:
    Bintree();
    Bintree(const Bintree& a);
    ~Bintree();
    void bildtree(const T& a);
    void show_tree();
    void add_subtree(Bintree<T>& a);
    void remove_subtree(const T& x);
    void remove_leaf(const T& x);
    vector<T> get_data(Leaf* l);
    Leaf* get_root()
    {
        return root;
    }
    Leaf* max_search()
    {
        return max_search_h(root);
    }
    Leaf* min_search()
    {
        return min_search_h(root);
    }
    Leaf* search_leaf(const T& x)
    {
        return search(x, root);
    }
};

template< typename T >
void Bintree< T >::remove_leaf(const T &x)
{
    return remove_leaf_h(root, x);
}
template< typename T >
void Bintree< T >::remove_leaf_h(Leaf *node, const T &x)
{
    Leaf* pointer = node;
    Leaf* parent = node->parent;
    while (pointer != nullptr && pointer->data != x)
    {
        parent = pointer;
        if (x < pointer->data)
            pointer = pointer->left;
        else
            pointer = pointer->right;
    }

    if (pointer != nullptr)
    {
        Leaf* removed = nullptr;
        if (pointer->left == nullptr || pointer->right == nullptr)
        {
            Leaf* child = nullptr;
            removed = pointer;
            if (pointer->left != nullptr)
                child = pointer->left;
            else
                if (pointer ->right != nullptr)
                    child = pointer->right;
            if (parent == nullptr)
                root = child;
            else
            {
                if (parent->left == pointer)
                {
                    parent->left = child;
                }
                else
                {
                    parent->right = child;
                }
            }
        }
        else
        {
            Leaf* mostLeft = pointer->right;
            Leaf* mostLeftParent = pointer;
            while (mostLeft->left != nullptr)
            {
                mostLeftParent = mostLeft;
                mostLeft = mostLeft->parent;
            }
            pointer->data = mostLeft->data;
            removed = mostLeft;
            if (mostLeftParent->left == mostLeft)
                mostLeftParent->left = nullptr;
            else
                mostLeftParent->right = mostLeft->right;
        }

        delete removed;
    }
}

template <typename T>
vector<T> Bintree<T>::get_data(Leaf* l)
{
    Leaf* left=l->left;
    Leaf* right=l->right;
    vector<T> dat;
    dat.push_back(l->data);
    while (left)
    {
        dat.push_back(left->data);
        left = left->left;
        if(left->right!=nullptr)
            dat.push_back(left->right->data);
    }
    while (right)
    {
        dat.push_back(right->data);
        right = right->right;
        if(right->left!=nullptr)
            dat.push_back(right->left->data);
    }

    return dat;
}

template <typename T>
void Bintree<T>::add_subtree(Bintree<T>& a)
{
    vector<T> add(a.get_data(a.get_root()));
    typename vector<T>::iterator it;
    it = add.begin();
    while (it != add.end())
    {
        bildtree(*it);
        ++it;
    }
}
template <typename T>
void Bintree<T>::remove_subtree(const T& x)
{
    Leaf* temp = search(x, root);
    if (root->data != x)
    {
        if (temp != nullptr)
        {
            Leaf* parent = temp->parent;
            if (parent != nullptr)
            {
                if (parent->left == temp)
                    parent->left = nullptr;
                else
                    if (parent->right == temp)
                        parent->right = nullptr;
            }
            if (temp != nullptr)
            {
                delet_tree(temp->left);
                delet_tree(temp->right);
                delete temp;
            }
        }
        else
            throw "Tree haven't subtree with root=data.";
    }
    else
        throw "It's root all tree. May be you want to delete tree than use other function.";
}
template <typename T>
Bintree<T>::Leaf::Leaf()
{
    data = nullptr;
    parent = nullptr;
    right = nullptr;
    left = nullptr;
}
template <typename T>
Bintree<T>::Leaf::Leaf(const T& b, Leaf* a)
{
    data = b;
    parent = a;
    right = nullptr;
    left = nullptr;
}
template <typename T>
Bintree<T>::Bintree()
{
    root = nullptr;
}
template <typename T>
Bintree<T>::~Bintree()
{
    delet_tree(root);
}
template<typename T>
void Bintree<T>::delet_tree(Leaf *i)
{
    if (i != nullptr)
    {
        if(i->left!=nullptr)
            delet_tree(i->left);
        if(i->right!=nullptr)
            delet_tree(i->right);
        delete i;
    }
}
template<typename T>
void Bintree<T>::bildtree(const T& a)
{
    return add_leaf(a);
}
template<typename T>
void Bintree<T>::add(const T& j, Leaf *root)
{
        if (root->data > j)
        {
            if (root->left == nullptr)
            {
                root->left = new Leaf(j, root);
            }
            else
            {
                add(j, root->left);
            }
        }
        if (root->data < j)
        {
            if (root->right == nullptr)
            {
                root->right = new Leaf(j, root);
            }
            else
            {
                add(j, root->right);
            }
        }
}
template <typename T>
void Bintree<T>::add_leaf(const T& value)
{
    Leaf *paren = nullptr;
    if (root == nullptr)
    {
        root = new Leaf(value, paren);
    }
    else
    {
        add(value, root);
    }
}
template <typename T>
void Bintree<T>::show_tree()
{
    if (root != nullptr)
        return h_show_tree(root, root->level);
    else
        throw "Emty tree.";
}
template <typename T>
void Bintree<T>::h_show_tree(Leaf *a, int level) const
{
    if (a != nullptr)
    {
        h_show_tree(a->left, level + 1);
        for (int i = 0; i < level; i++)
            cout << '-';
        cout << '<' << a->data << '>' << endl;
        h_show_tree(a->right, level + 1);
    }

}

#endif // MYTREE_H
