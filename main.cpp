#include "mytree.h"
#include <string>
#include <ctime>

int main()
{
    Bintree<int> A;
    Bintree<int> B;
    //string str;
    srand(unsigned(time(nullptr)));
    for (int i = 0; i < 10; i++)
    {
        A.bildtree(rand()%10);
        B.bildtree(rand()%5);
    }
    int x;
    try
    {
        cout << "Tree A:" << endl;
        A.show_tree();
        cout << endl;
        //A.min_search();
        //A.max_search();
        cout << "Tree A without subtree:" << endl;
        A.remove_subtree(8);
        A.show_tree();
        cout << endl;
        cout << "Tree B:" << endl;
        B.show_tree();
        cout << endl;
        cin >> x;
        cout << "Tree A without element "<< x <<" :" << endl;
        A.remove_leaf(x);
        A.show_tree();
        cout << endl;
        cout << "Tree A with add tree B:" << endl;
        A.add_subtree(B);
        A.show_tree();
    }
    catch (const char* t)
    {
        cout <<t<< endl;
    }
    cout << endl;
    system("pause");
    return 0;
}

